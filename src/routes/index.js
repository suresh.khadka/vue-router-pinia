import { createRouter, createWebHistory } from "vue-router"

// import Home from "../views/Home.vue";
// import About from "../views/About.vue";
// import Movie from '../views/Movie.vue';

// const Home = () => import("../views/Home.vue");
// const About = () => import("../views/About.vue");
// const Movie = () => import("../views/Movie.vue");

const routes = [
  { path: "/", name: "home", component: () => import("../views/Home.vue")},
  { path: "/about", name: "about", component: () => import("../views/About.vue") },
  { path: "/movie", name: "movie", component: () => import("../views/Movie.vue")},
  { path: "/movie/:id", name: "movie.edit", component: () => import("../views/MovieDetail.vue"), props:true},
  { path: "/:pathMatch(.*)*", name: "notfound", component: () => import("../views/404.vue")}
];

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router;